from datetime import datetime
from typing import Dict
from os import environ
from injector import inject, singleton
from orcaai_core import AsyncPostgresClient
from sqs_provider import SQSProvider
from file_status import FileStatus


@singleton
class Manager:
    @inject
    def __init__(self, postgres_client: AsyncPostgresClient, sqs_provider: SQSProvider):
        self._postgres_client = postgres_client
        self._sqs_client = sqs_provider
        self._queue_url = environ.get("SQS_URL")
        self._bucket_url = environ.get("BUCKET_URL")
        self._date_format = "%y_%m_%d_%H_%M_%S"
        self._files_table = environ.get("FILES_TABLE")

    async def connect(self) -> None:
        await self._postgres_client.connect()

    async def disconnect(self) -> None:
        await self._postgres_client.close()

    def parse_event(self, event: Dict[str, str]) -> Dict[str, str]:
        file_relative_path = event.get("s3").get("object").get("key")
        split_path = file_relative_path.split("/")
        ship_name = split_path[1]
        file_date = datetime.strptime(split_path[-1][:-5], self._date_format)
        file_path = f"{self._bucket_url}/{file_relative_path}"

        return {
            "ship_name": ship_name,
            "file_date": file_date,
            "relative_path": file_relative_path,
            "file_path": file_path,
            "file_processed": FileStatus.UNPROCESSED.value,
        }

    async def add_file_to_postgres(self, file_data: Dict[str, str]) -> None:
        file_path = file_data["file_path"]
        ship_name = file_data["ship_name"]
        file_date = file_data["file_date"]
        file_processed = file_data["file_processed"]

        query = (
            f"INSERT INTO {self._files_table} ("
            f"path, ship, date_updated, process_status)"
            f"VALUES ('{file_path}', '{ship_name}', '{file_date}', '{file_processed}')"
        )
        await self._postgres_client.transaction(query)

        row_id_query = f"SELECT CURRVAL('{self._files_table}_id_seq')"
        row = await self._postgres_client.select_one(row_id_query)
        file_data["row_id"] = row[0]

    def send_event_to_queue(self, file_data: Dict[str, str]) -> None:
        date_as_datetime = file_data["file_date"]
        file_data["file_date"] = date_as_datetime.isoformat()

        self._sqs_client.send_message(self._queue_url, file_data)

    async def run(self, event: Dict[str, str]) -> None:
        await self.connect()

        for record in event.get("Records"):
            file_data = self.parse_event(record)
            await self.add_file_to_postgres(file_data)
            self.send_event_to_queue(file_data)

        await self.disconnect()
