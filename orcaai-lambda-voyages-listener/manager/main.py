import asyncio
import json
from injector import Injector
from core_module import CoreModule
from manager import Manager


def main(event, context):
    ioc_container = Injector(modules=[CoreModule])
    manager = ioc_container.get(Manager)
    asyncio.run(manager.run(event))

    return {"statusCode": 200, "body": "Event processed"}
