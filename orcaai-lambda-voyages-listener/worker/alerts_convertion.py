from os import environ
from typing import Dict
from injector import singleton


@singleton
class AlertsConvertor:
    def __init__(self):
        self._convertor = {
            "dangerous_below_keel_risk_level": "dangerous_below_keel",
            "dangerous_below_transducer_risk_level": "dangerous_below_transducer",
            "speed_drop_risk_level": "speed_drop",
            "dangerous_motion_risk_level": "dangerous_motion",
            "dangerous_rot_risk_level": "dangerous_rot",
            "collision_risk_level": "collision_risk",
        }
        self._alerts_table = environ.get("ALERTS_TABLE")

    def convert(self, alert):
        return self._convertor[alert]

    def create_insert_query(self, ship_name: str, alerts_count: Dict[str, int]) -> str:
        alerts_names_query = f"ship"
        alerst_values_query = f"VALUES (\'{ship_name}\'"
        for alert_name, value in alerts_count.items():
            alerts_names_query += f", {alert_name}"
            alerst_values_query += f", {value}"

        query = f"INSERT into {self._alerts_table}({alerts_names_query}) {alerst_values_query})"
        return query

    def create_update_query(self, ship_name, alerts_count, ship_row):
        ship_dict = {**ship_row}
        for key in alerts_count:
            if key in ship_dict:
                alerts_count[key] += ship_dict[key]
            else:
                raise Exception("alert not found in table")

        query = f"UPDATE {self._alerts_table} SET"
        for key in alerts_count:
            query += f" {key}={alerts_count[key]},"
        query = query[:-1] + f" WHERE ship='{ship_name}'"
