import json
import boto3
import os
from injector import inject, singleton
from orcaai_core import AsyncPostgresClient
from typing import Dict
from file_status import FileStatus
from worker.alerts_convertion import AlertsConvertor


@singleton
class Worker:
    @inject
    def __init__(
        self, postgres_client: AsyncPostgresClient, alerts_convertor: AlertsConvertor
    ):
        self._postgres_client = postgres_client
        self._alerts_convertor = alerts_convertor
        self._bucket_name = os.environ.get("BUCKET_NAME")
        self._s3_client = boto3.client("s3")
        self._file_name = "logs_file"
        self._files_table = os.environ.get("FILES_TABLE")
        self._alerts_table = os.environ.get("ALERTS_TABLE")

    async def connect(self) -> None:
        await self._postgres_client.connect()

    async def disconnect(self) -> None:
        await self._postgres_client.close()

    async def _set_record_process_status(self, row_id: int, status: str) -> None:
        update_query = (
            f"UPDATE {self._files_table}"
            f" SET process_status='{status}' WHERE id = {row_id}"
        )
        await self._postgres_client.transaction(update_query)

    async def check_file_record(self, file_data: Dict[str, str]) -> bool:
        row_id = file_data["row_id"]
        read_query = f"SELECT * from {self._files_table} WHERE id = {row_id}"
        row = await self._postgres_client.select_one(read_query)
        if row[4] != FileStatus.UNPROCESSED.value:
            return False
        await self._set_record_process_status(row_id, FileStatus.IN_PROGRESS.value)
        return True

    def _read_file(self, file_data: Dict[str, str]) -> Dict[str, str]:
        with open(self._file_name, "wb") as data:
            self._s3_client.download_fileobj(
                self._bucket_name,
                f'data{file_data["relative_path"]}',
                data,
            )

        with open(self._file_name, "r") as file:
            file = json.loads(file.read())
        return file

    async def _add_alerts_to_table(
        self, file_data: Dict[str, str], alerts_count: Dict[str, int]
    ) -> None:
        ship_name = file_data["ship_name"]
        get_row_query = f"SELECT * from {self._alerts_table} WHERE ship ='{ship_name}'"
        ship_row = await self._postgres_client.select_one(get_row_query)
        if ship_row:
            query = self._alerts_convertor.create_update_query(ship_name, alerts_count, ship_row)
        else:
            query = self._alerts_convertor.create_insert_query(ship_name, alerts_count)
        await self._postgres_client.transaction(query)

    def get_alerts_from_file(self, file_data: Dict[str, str]) -> Dict[str, int]:
        file = self._read_file(file_data)
        alerts = {
            **file["system_state"]["shore_state"]["current_env_alerts_state"],
            **file["system_state"]["shore_state"]["current_target_alerts_state"],
        }
        alerts_count = {}
        for alert_type, risk in alerts.items():
            converted_alert = self._alerts_convertor.convert(alert_type)
            alerts_count[converted_alert] = 1 if risk == "High" else 0
        return alerts_count

    async def run(self, event):
        await self.connect()

        for record in event.get("Records"):
            file_data = json.loads(record["body"])
            is_file_need_process = await self.check_file_record(file_data)
            if is_file_need_process:
                try:
                    alerts = self.get_alerts_from_file(file_data)
                    await self._add_alerts_to_table(file_data, alerts)
                    await self._set_record_process_status(
                        file_data["row_id"], FileStatus.DONE.value
                    )
                except Exception:
                    self._set_record_process_status(
                        file_data["row_id"], FileStatus.FAILED.value
                    )

        try:
            os.remove(self._file_name)
        except Exception:
            pass
        await self.disconnect()
