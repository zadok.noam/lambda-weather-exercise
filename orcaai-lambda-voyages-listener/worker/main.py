import asyncio
import json
from injector import Injector
from core_module import CoreModule
from worker.alerts_convertion import AlertsConvertor
from worker import Worker


def main(event, context):

    ioc_container = Injector(modules=[CoreModule])
    worker = ioc_container.get(Worker)
    asyncio.run(worker.run(event))

    return {"statusCode": 200, "body": "Event processed"}
