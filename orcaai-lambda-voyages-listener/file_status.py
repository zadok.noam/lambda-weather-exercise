from enum import Enum


class FileStatus(Enum):
    UNPROCESSED = "Unprocessed"
    IN_PROGRESS = "InProgress"
    FAILED = "Failed"
    DONE = "Done"
