import json
from os import environ
import boto3
from typing import Dict
from injector import singleton

@singleton
class SQSProvider:
    def __init__(self):
        self._client = boto3.client(
            "sqs",
            aws_access_key_id=environ.get("ACCESS_KEY_ID"),
            aws_secret_access_key=environ.get("SECRET_ACCESS_KEY"),
        )

    def send_message(self, queue_url: str, message: Dict[str, str]) -> None:
        self._client.send_message(
            QueueUrl=queue_url, MessageBody=json.dumps(message)
        )
