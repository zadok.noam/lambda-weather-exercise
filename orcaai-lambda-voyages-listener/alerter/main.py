import asyncio
import json
from injector import Injector
from core_module import CoreModule
from alerter import Alerter


def main(event, context):

    ioc_container = Injector(modules=[CoreModule])
    alerter = ioc_container.get(Alerter)
    asyncio.run(alerter.run(event))

    return {"statusCode": 200, "body": "Event processed"}