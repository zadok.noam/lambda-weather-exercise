import json
from os import environ
from injector import inject, singleton
from orcaai_core import AsyncPostgresClient
from file_status import FileStatus


@singleton
class Alerter:
    @inject
    def __init__(self, postgres_client: AsyncPostgresClient):
        self._postgres_client = postgres_client
        self._files_table = environ.get("FILES_TABLE")

    async def _mark_event_as_failed(self, file_data):
        row_id = file_data["row_id"]
        update_query = (
            f"UPDATE {self._files_table}"
            f" SET process_status='{FileStatus.FAILED.value}' WHERE id = {row_id}"
        )
        await self._postgres_client.transaction(update_query)


    async def connect(self) -> None:
        await self._postgres_client.connect()

    async def disconnect(self) -> None:
        await self._postgres_client.close()


    async def run(self, event):
        await self.connect()

        for record in event.get("Records"):
            file_data = json.loads(record["body"])
            await  self._mark_event_as_failed(file_data)

        await self.disconnect()
